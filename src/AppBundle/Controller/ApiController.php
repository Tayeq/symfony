<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiController extends Controller
{
    /**
     * @Route("/api", name="api_index")
     */
    public function index( Request $request)
    {
        $commands = [
            'articles', 'article/add'
        ];
        return new JsonResponse($commands);
    }

    /**
     * @Route("/api/article/add", name="api_article_add")
     * @Method({"POST","GET"})
     */
    public function addArticle(Request $request)
    {
        $text = $request->get('text');
        $addedBy = $request->get('added_by');

        if(!$text){
            throw new \Exception('Text parameter is required!');
        }
        if(!$addedBy){
            throw new \Exception('Added_by parameter is required!');
        }

        $em = $this->getDoctrine()->getManager();

        $article = new Article();
        $article->setText($text);
        $article->setAddedBy($addedBy);
        $article->setAddedDate(new \DateTime("now"));

        $em->persist($article);

        $em->flush();

        return new JsonResponse('New article id: '.$article->getId());
    }

    /**
     * @Route("/api/article/delete/{articleId}", name="api_article_delete", requirements={"articleId": "\d+"})
     */
    public function deleteArticle($articleId)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($articleId);

        if (!$article) {
            throw $this->createNotFoundException(
                'Article with id: '.$articleId.' not found '
            );
        }

        $em->remove($article);
        $em->flush();

        return new JsonResponse('Article with id: '.$articleId. ' removed');

    }

    /**
     * @Route("/api/articles", name="api_articles")
     */
    public function articles()
    {
        $articles = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findAll();

        if (!$articles) {
            throw $this->createNotFoundException(
                'Articles not found'
            );
        }
        $serializer = $this->get('serializer');
        $jsonContent = $serializer->serialize($articles, 'json');

        return new Response($jsonContent);
    }

    /**
     * @Route("/api/article/{articleId}", name="api_article_get", requirements={"articleId": "\d+"})
     */
    public function article($articleId)
    {
        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->find($articleId);

        if (!$article) {
            throw $this->createNotFoundException(
                'Article with id: '.$articleId.' not found '
            );
        }
        $serializer = $this->get('serializer');
        $jsonContent = $serializer->serialize($article, 'json');

        return new Response($jsonContent);
    }
    //@TODO ARTICLEEDIT
}
